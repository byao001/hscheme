# README #
-------------------------------------

A Scheme interpreter written in Haskell, started for learning Haskell.

The code follows the tutorial "Write Yourself a Scheme in 48 Hours", and finished all the exercises.

The tutorial can be find here: https://en.wikibooks.org/wiki/Write_Yourself_a_Scheme_in_48_Hours.

The GHC version is 7.10.3.
